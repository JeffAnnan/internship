#!/usr/bin/env python

vertices = []
faces = []

#split the raw file. Separate vertices and faces in different lists
with open('frog10v.obj', 'r') as f: 
    for line in f:
        data = line.split()
        if data[0] == 'v' in line:
            vertices.append(line)
        if data[0] == 'f' in line:
            faces.append(line)
nbVertices = len(vertices)
nbFaces = len(faces)

print('  ________________________________')
print('_/ ==[*mesh info]====[splitter]== \_______________')
print('|  ',nbVertices,': vertices')
print('|  ',nbFaces,': faces')
print('|_________________________________________________')

endPrevious = 0
endPreviousList = []
beginNext = 0
beginNextList = []
thrustRate = 0
thrustRateList = []

cellsBorders = []

print("Search volumes in progress (1/2)...")
#detecting an sorting the borders of different volumes 
for i in range(nbVertices):
    listOfFind = []
    for face in faces:
        pos = face.find(' '+str(i+1)+' ')
        if pos != -1:
            listOfFind.append(face)
            
    
    endPrevious = faces.index(listOfFind[len(listOfFind)-1]) #last index of the number found
    endPreviousList.append(endPrevious) 

    beginNext = faces.index(listOfFind[0]) #first index of the number found
    beginNextList.append(beginNext)

    print (i,"/",nbVertices," vertices",end="\r")

beginNextList.pop(0)

print("Search volumes in progress (2/2)...")
#countVolumeFound = 0
#list of the cells borders
for i in range(len(beginNextList)):
    if endPreviousList[i] > thrustRate:
        thrustRate = endPreviousList[i]
    if beginNextList[i] > endPreviousList[i] and beginNextList[i] > thrustRate:
        #countVolumeFound += 1
        #print("Volume ",countVolumeFound, "found",end="\r")

        index = beginNextList[i]
        cellsBorders.append(index)
#countVolumeFound += 1
#print("Volume ",countVolumeFound, "found",end="\r")
print("Preparation of volumes in progress...")
#countVolumeReady = 0
start = 0
listOfVolumes = []
for cut in cellsBorders:
    constructTheVolume = []
    for split in range(start, cut):
        constructTheVolume.append(faces[split])
    #countVolumeReady += 1
    #print("Volume ",countVolumeReady,"/",countVolumeFound," ready" )
    listOfVolumes.append(constructTheVolume)
    start = cut

constructTheLastVolume = []
for lastCell in range (cellsBorders[-1], len(faces)):
    constructTheLastVolume.append(faces[lastCell])

#print("Volume ",countVolumeReady+1,"/",countVolumeFound," ready" )
listOfVolumes.append(constructTheLastVolume)

#print(listOfVolumes)
#print(len(listOfVolumes))

#constructing the vertices list correponding to the volume list. Each index correspond to same index in the list of volumes 
listOfVolumesVertices = []
listOfMinimumVertexIndicePerVolume = []
for volume in listOfVolumes: 

    v = [] #vertices index in a face line
    for volumeVertices in volume:
        cursorVertices = volumeVertices.split() #split vertices for a face 
        for splitLine in cursorVertices:
            if splitLine != 'f':
                v.append(int(splitLine))
    v = list(dict.fromkeys(v)) #remove duplicates vertices
    v.sort() #sort the vertices list  
    mini = min(v) #find the minimum vertex indice to normalize the others
    listOfMinimumVertexIndicePerVolume.append(mini)
    constructTheVolumeVertices = []
    for injectVertices in v:
        constructTheVolumeVertices.append(vertices[injectVertices-1])
    listOfVolumesVertices.append(constructTheVolumeVertices)

#print(listOfVolumesVertices[0])

print("Preparation of vertices in progress...")
#normalization of face vertices
#print(listOfMinimumVertexIndicePerVolume)
listOfVolumesNormalized = []
countMini = 0
for volumeToNormalize in listOfVolumes: 
    constructTheVolumeNormalized = []
    constructTheVolumeNormalizedString = ""
    vert = [] #vertices index in a face line
    for volumeVert in volumeToNormalize:
        cursorVertices = volumeVert.split() #split vertices for a face 
        for split in cursorVertices:
            if split != 'f':
                vert.append(int(split))
        normalized = [x - (listOfMinimumVertexIndicePerVolume[countMini]-1) for x in vert] #vertices list normalized       
        string = "f"
        for verticesInteger in normalized:
            string += " " + str(verticesInteger)  
        constructTheVolumeNormalized.append(string)
        vert.clear()
    listOfVolumesNormalized.append(constructTheVolumeNormalized)
    countMini += 1

print("Ready")
print(len(listOfVolumes), "volumes in volume list")
print(len(listOfVolumesVertices), "volumes in vertices list")
print(len(listOfVolumesNormalized), "volumes in volume normalized list")
print

#verification to see the coherence and write files
verif1 = len(listOfVolumes)
verif2 = len(listOfVolumesVertices)
verif3 = len(listOfVolumesNormalized)
if verif1 == verif2 == verif3:
    print("Ready to write files")
    for indexVolume in range(verif2):
        path = "/Users/joffreyannan/Desktop/output/voroVolume"
        fileName =  path + str(indexVolume+1) + ".obj"
        with open(fileName, 'w') as of:
            for outputVertices in listOfVolumesVertices[indexVolume]:
                of.write(outputVertices)
            of.write('\n')
            for outputFaces in listOfVolumesNormalized[indexVolume]:
                of.write(outputFaces+'\n')
    print("Done !")     
else:
    print("Error, no coherence volumes")


