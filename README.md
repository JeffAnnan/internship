## Welcome to GitLab internship repository

Subject : **Voronoi partitions** or **Dirichlet tessellation** in complex convex shapes.

## Table of Contents 
* [Week 1](#week-1)
* [Week 2](#week-2)
* [Week 3](#week-3)
* [Week 4](#week-4)
* [Week 5](#week-5)
* [Week 6](#week-6)
* [Week 7](#week-7)
* [Week 8](#week-8)
* [About Graphite](#about-graphite)
* [About customized GEOGRAM / Splitter ./vorpalite command](#about-customized-geogram-splitter-with-vorpalite-commmand)
* [About customized GEOGRAM / Splitter ./compute_RVD command](#about-customized-geogram-splitter-with-compute-rvd-commmand)
* [About GEOGRAM & Tetrahedralization](#about-geogram-and-tetrahedralization)
* [About GEOGRAM2GranOO](#about-geogram2granoo)
* [About GranOO and viewer triangulation](#about-granoo-and-viewer-triangulation)
* [Benchmark](#benchmark)
* [Bibliography](#bibliography)

## week 1
* Prospecting and Documentation
* 3d tests : OpenSCAD, MeshLab
* Lines of prospecting : 
    * Voronoi partitions 
    * Poisson-disc sampling 
* Tetgen & TetView prospecting
* Voro++ prospecting
* Make & Cmake : automate the compilation process
* Medit prospecting
* Qfull prospecting
* NetGen prospecting 

## week 2
* Geogram prospecting about Voronoi tessellation
* Geogram + graphite prospecting about Voronoi tessellation

## week 3
* Coding a mesh splitter : with Python
* Customize GEOGRAM to split meshs

## week 4
* Customize GEOGRAM to split meshs - end ✓

## week 5
* Coding a plugin for Granoo to import .off splitted mesh
    * Fixing rendering bugs
        * Solution 1 : facetizing the mesh even the outer envelope
        * Solution 2 : fixing viewer bugs with GranOO
            * Face Culling of triangles :
                * <a href="https://www.khronos.org/opengl/wiki/Face_Culling" target="_blank">OpenGL Face Culling Wiki</a> 
                * <a href="https://learnopengl.com/Advanced-OpenGL/Face-culling" target="_blank">OpenGL Face Culling Tutorial</a> 
* Improve voronoi tessellations from GEOGRAM
    * Prospecting using __./comput_RVD__ command instead of __./vorpalite__ : rendering quite good ✓
        * coding a mesh splitter from GEOGRAM and __./compute_RVD__ command : done ✓

## week 6
* Improve rendings on GranOO : concerning the triangulation of NON-CONVEX faces (see [About GranOO and viewer triangulation](#about-granoo-and-viewer-triangulation))
* Documentation : write documentation about that triangulation problem.
* Tests on GranOO with tessellations : tests tessellations with `compute_RVD` command and see the results on granOO

## week 7
* Coding the connectivity table of all voronoi cells : trying with GEOGRAM but did not succeed to use the following methods : 
    * adjacent (index_t c, index_t lf) (Gets a cell adjacent to another one by local facet index) : <a href="http://alice.loria.fr/software/geogram/doc/html/classGEO_1_1MeshCells.html#a36b6399f9a0b836f365565922301770e" target="_blank">here</a>
    * <a href="http://alice.loria.fr/software/geogram/doc/html/classGEO_1_1RestrictedVoronoiDiagram.html#a7daa02802464ae94806dbbbca93a0556" target="_blank">after a Restricted Voronoi Digram computed by GEOGRAM (using delaunay ()),</a> <a href="http://alice.loria.fr/software/geogram/doc/html/classGEO_1_1Delaunay.html#a3d3b2a48a152d2e1e6edf7bb8fca6c9e" target="_blank">using get_neighbors (index_t v, vector< index_t > &neighbors) </a>
    * Waiting for help from INRIA...
* Simulations with GranOO and voronoi meshs. GranOO errors for 20000 or 50000 cells meshs due to null masses.

## week 8
* Simulations with GranOO and voronoi meshs.

## about Graphite  
* <a href="https://gforge.inria.fr/frs/?group_id=1465" target="_blank">Download it</a>
* <a href=http://alice.loria.fr/software/graphite/doc/html/>Documentation</a>
* Generate a voronoi mesh : 
    * File..>open_you_envelope_file_[.off / .stl]
    * Right click on your open file below 'Scene'
    * Volume > Voronoi meshing > set_your_meshing [nb cells / shrink / ...] > ✓
    * To export your Voronoi mesh : right click on the generated 'voronoi' file. File..>Save as..>select_your_favorite_extension. That's it
 
## about customized geogram splitter with **vorpalite** commmand
* This version of Geogram enable to split in (__outer envelope non facetized__) polyhedrons a general mesh computed in Voronoi diagram.
* It is based on the 1.7.5 GEOGRAM version
* How to use it : 
    * In the binary directory use the following command : `./vorpalite my_file_to_mesh.stl poly=true poly:simplify=tets_voronoi_boundary /path/to/save/splitted/volumes/my_output_file.off pts=10` 
    * my_output_file.off is the aggregated output file. The folder containing all splitted volumes will be created next to this file.
    * poly=true -> generate polyhedrons
    * poly:simplify=tets_voronoi_boundary -> simplify boundaries in meshing process
    * pts=10 -> to choose how many cells we want
* Try this command with envelope mesh that you can find in the 'Envelope_bench' folder
* Find the source code in the 'Customized_Geogram' folder
* All changes to the original GEOGRAM project have been made to the following files : 
    * `/geogram_1.7.5/src/bin/vorpalite/main.cpp`
    * `/geogram_1.7.5/src/lib/geogram/voronoi/RVD_callback.h`
    * `/geogram_1.7.5/src/lib/geogram/voronoi/RVD_callback.cpp`
    * Notice that you can find the changes under the '[SPLITTERvorpalite]' comments in these files.
    * <a href="http://alice.loria.fr/software/geogram/doc/html/annotated.html" target="_blank">GEOGRAM classes documentation</a>
* Find a picture in the following section about what cutomized GEOGRAM do.

## about customized geogram splitter with **compute rvd** commmand
* This version of Geogram enable to split in (__outer envelope facetized__) polyhedrons a general mesh computed in Voronoi diagram.
* It is based on the 1.7.5 GEOGRAM version
* How to use it : 
    * In the binary directory use the following command : `./compute_RVD meshfile.off pointsfile.xyz volumetric=true RVD_cells=true cell_borders=true RVD_cells:simplify_boundary=true /path/to/save/splitted/volumes/my_output_file.off`
    * my_output_file.off is the aggregated output file. The folder containing all splitted volumes will be created next to this file.
    * volumetric=tru -> to use the API for computing RVD cells
    * RVD_cells=true -> to use the API for computing RVD cells
    * cell_borders=true -> to generate only cell border
    * RVD_cells:simplify_boundary -> Simplify boundary facets
* Try this command with envelope mesh that you can find in the 'Envelope_bench' folder
* Find the source code in the 'Customized_Geogram' folder
* All changes to the original GEOGRAM project have been made to the following files : 
    * `/geogram_1.7.5_splitter/src/examples/geogram/compute_RVD/main.cpp`
    * Notice that you can find the changes under the '[SPLITTERcompute_RVD]' comments in these files.
* Find a picture in the following section about what cutomized GEOGRAM do

## about geogram and tetrahedralization 
* If you want to remesh an envelope file (.off, .stl...) into tetrahedrons you may use the following command : 
    * `./vorpalite my_envelope_file_.off /where/I/save/the/output/file/my_output_file.off pts=1000`
    * pts=1000 ->number of tetrahedrons you want to obtain
    * Find a picture in the following section about the result.

## about geogram2granoo
* How to use it :
    * Copy and paste your folder containing all splitted meshs and rename it 'outputSplitter'
    * Compile the plugin
    * Launch the command : `./build/convert.exe ./run.inp`
    * Find this plugin in the GranOO folder 

## about granoo and viewer triangulation
* This section aims at referencing somes problems or singularities concerning the visualization of **NON-CONVEX** and **NON PRE-TRIANGULATED** polygons in GranOO. 
* The singularity is as follows : 
    * GranOO triangulates each faces of each polyhedron to display them in the GranOO Viewer and so make volumes : <a href="benchmark/Capture_d_écran_2020-06-24_à_11.53.51.png" target="_blank">like this</a>.
    * __But__ singularities appear when GranOO triangulates __NON-CONVEX__ faces : <a href="benchmark/granOOTriangulationSingularity.pdf" target="_blank">like this</a>. For mor complexe cases it leads to <a href="benchmark/moreComplexSingularities.pdf" target="_blank">that</a>. 
    * You can find the simple example mesh in the 'granoo' folder under the 'granOO-triangulation-singularity' folder in .off and .lgdd.
* So here are some tips to fix the problem. This consists in triangulating each face of a **NON-CONVEX** polyhedron by GranOO. Modifications could be made in this file : granoo/tags/2.0/Lib/GranOO/libAlgo/PolyhedronCore.cpp.
    > * <a href="https://www.wykobi.com/tutorial.html#ConcavePolygonClippedAgainstARectangle" target="_blank">Use a C++ oriented computational geometry library for triangulation: Wykobi</a> 
    > * <a href="https://dzhelil.info/triangle/" target="_blank">Use Python library called 'Triangle' to operate triangulation</a><a href="https://www.labri.fr/perso/nrougier/python-opengl/" target="_blank"> and be used like this 10.1.2 section</a>
    > * <a href="https://github.com/ivanfratric/polypartition" target="_blank">Use a C++ library</a>
    > * <a href="https://github.com/mapbox/earcut.hpp" target="_blank">Use a C++ library</a>
    > * <a href="https://stackoverflow.com/questions/1418831/c-2d-tessellation-library" target="_blank">Use CGAL libraries for 2D tesselation</a><a href="https://doc.cgal.org/latest/Partition_2/group__PkgPartition2Ref.html#ga3ca9fb1f363f9f792bfbbeca65ad5cc5" target="_blank"> like this</a><a href="https://doc.cgal.org/latest/Partition_2/index.html" target="_blank"> or 2D CGAL Polygon Partitioning</a>
    > * <a href="http://gamma.cs.unc.edu/SEIDEL/#Lischinski" target="_blank">Use : Fast Polygon Triangulation Based on Seidel's Algorithm</a>
    > * <a href="http://www.personal.kent.edu/~rmuhamma/Compgeometry/MyCG/PolyPart/polyPartition.htm" target="_blank">Use this polygon partitioning explanation</a>
    > * <a href="https://fr.coursera.org/lecture/computational-geometry/4-7-triangulating-a-monotone-polygon-5SeFC" target="_blank">Other resources</a><a href="https://www.gamedev.net/tutorials/programming/graphics/polygon-triangulation-r3334/" target="_blank"> Other resources</a><a href="https://sites.cs.ucsb.edu/~suri/cs235/Triangulation.pdf" target="_blank"> Other resources</a><a href="https://stackoverflow.com/questions/45226931/triangulate-a-set-of-points-with-a-concave-domain" target="_blank"> Other resources</a><a href="https://en.wikipedia.org/wiki/Polygon_triangulation" target="_blank"> Other resources</a><a href="https://www.racoon-artworks.de/cgbasics/triangulation.php" target="_blank"> Other resources</a>
    > _________________
    > * It could be good to know and verify if a point (a point forming part of an edge) is inside or not a polygon and thus know if an edge is part of the general mesh.
    > * <a href="http://paulbourke.net/geometry/polygonmesh/" target="_blank">Determining whether a line segment intersects a 3 vertex facet</a>
    > * <a href="http://erich.realtimerendering.com/ptinpoly/" target="_blank">Point in Polygon Strategies : notice a way to go from a **'3D to a 2D'** system</a>
    > * <a href="https://www.geeksforgeeks.org/how-to-check-if-a-given-point-lies-inside-a-polygon/" target="_blank">How to check if a given point lies inside or outside a polygon?</a>
    > * <a href="http://alienryderflex.com/polygon/" target="_blank">Point-In-Polygon Algorithm — Determining Whether A Point Is Inside A Complex Polygon</a>
    > * <a href="http://www.eecs.umich.edu/courses/eecs380/HANDOUTS/PROJ2/InsidePoly.html" target="_blank">Determining if a point lies on the interior of a polygon</a>
    > * <a href="https://www.codeproject.com/Articles/371959/A-brute-force-approach-to-check-if-a-line-segment" target="_blank">A brute-force approach to check if a line segment crosses a simple polygon</a>
    > * <a href="http://csharphelper.com/blog/2014/07/determine-whether-a-point-is-inside-a-polygon-in-c/" target="_blank">Determine whether a point is inside a polygon in C#</a>
    > * <a href="https://www.codeproject.com/Articles/1065730/Point-Inside-Convex-Polygon-in-Cplusplus" target="_blank">Point Inside 3D Convex Polygon in C++</a>
   
## Benchmark
###  Customized GEOGRAM **compute_RVD** 
> * ![alt text](benchmark/Capture_d_écran_2020-06-18_à_14.18.44.png "2 compute_RVD to GranOO") 
> * ![alt text](benchmark/Capture_d_écran_2020-06-18_à_14.00.16.png "1 compute_RVD in GEOGRAM") 
### About GEOGRAM & Tetrahedralization 
> * ![alt text](benchmark/Capture_d_écran_2020-06-15_à_14.37.23.png "1 About GEOGRAM & Tetrahedralizatio") 
###  Customized GEOGRAM **vorpalite** 
> * ![alt text](benchmark/splitterBench.png "1 Customized GEOGRAM") 
> * ![alt text](benchmark/fragment_of_211_splitted_cells.png "2 Fragment of 211 splitted cells with GEOGRAM")
> * ![alt text](benchmark/Capture_d_écran_2020-06-12_à_09.48.03.png "3 Fragment of splitted cells with GEOGRAM") 
###  Graphite - Voronoi Meshing
 > * ![alt text](benchmark/Capture_d_écran_2020-05-25_à_12.00.04.png "7 Graphite + Geogram") 
 > * ![alt text](benchmark/Capture_d_écran_2020-05-25_à_10.34.33.png "6 Graphite + ParaView .ply") 
 > * ![alt text](benchmark/Capture_d_écran_2020-05-25_à_10.02.38.png "5") 
 > * ![alt text](benchmark/Capture_d_écran_2020-05-25_à_10.00.21.png "4")
 > * ![alt text](benchmark/Capture_d_écran_2020-05-25_à_09.57.44.png "3")
 > * ![alt text](benchmark/Capture_d_écran_2020-05-25_à_09.39.22.png "2")
 > * ![alt text](benchmark/Capture_d_écran_2020-05-25_à_09.36.30.png "1")
###  Geogram - Voronoi Meshing
 > * ![alt text](benchmark/Capture_d_écran_2020-05-20_à_16.45.19.png "3 vorpalite profile=poly")
 > * ![alt text](benchmark/Capture_d_écran_2020-05-20_à_16.52.44.png "2 vorpalite profile=poly")
 > * ![alt text](benchmark/Capture_d_écran_2020-05-25_à_07.58.01.png "1 compute RVD")
###  TetGen + Gmsh - Tetrahedral Meshing
 > * ![alt text](benchmark/Capture_d_écran_2020-05-08_à_08.28.24.png "1")
### Netgen - Tetrahedral Mesh
 > * ![alt text](benchmark/Capture_d_écran_2020-05-08_à_10.52.18.png "1")
### TetGen + Medit - Tetrahedral Meshing
 > * ![alt text](benchmark/Capture_d_écran_2020-05-07_à_15.04.43.png "7")
 > * ![alt text](benchmark/Capture_d_écran_2020-05-07_à_15.03.55.png "6") 
 > * ![alt text](benchmark/Capture_d_écran_2020-05-07_à_08.47.42.png "5")
 > * ![alt text](benchmark/Capture_d_écran_2020-05-07_à_08.48.59.png "4")
 > * ![alt text](benchmark/Capture_d_écran_2020-05-07_à_08.53.07.png "3")
 > * ![alt text](benchmark/Capture_d_écran_2020-05-07_à_08.37.53.png "2")
 > * ![alt text](benchmark/Capture_d_écran_2020-05-07_à_08.32.23.png "1")

### TetGen + ParaView - Tetrahedral Meshing
 > * ![alt text](benchmark/Capture_d_écran_2020-05-05_à_14.44.36.png "1")
 > * ![alt text](benchmark/Capture_d_écran_2020-05-05_à_14.42.37.png "2")
 > * ![alt text](benchmark/Capture_d_écran_2020-05-05_à_14.44.46.png "3")
 > * ![alt text](benchmark/Capture_d_écran_2020-05-05_à_14.41.38.png "4")

## bibliography
### concepts
   > * <a href="https://www.college-de-france.fr/site/jean-daniel-boissonnat/course-2017-03-29-17h00.htm" target="_blank">Cours Modèles géométriques discrets - Collège de France - 29'24" to 44'10"</a>
   > * <a href="http://www.tsi.telecom-paristech.fr/pages/enseignement/ressources/beti/delaunay/delaunay_imprimable.pdf" target="_blank">La triangulation de Delaunay: application au problème de la superresolution pdf</a>
   > * <a href="http://www.cemyuksel.com/cyCodeBase/soln/poisson_disk_sampling.html" target="_blank">Poisson Disk Sampling</a>
   > * <a href="https://medium.com/coding-blocks/make-and-cmake-automating-c-build-process-900f569a75db" target="_blank">Make and CMake : Automating C++ Build Process</a>
### definitions
   > * <a href="https://fr.wikipedia.org/wiki/Triangulation_de_Delaunay" target="_blank">Triangulation de Delaunay</a>
   > * <a href="https://fr.wikipedia.org/wiki/Simplexe" target="_blank">Simplexe</a>
### installations & setup
   > * <a href="https://github.com/ISCDtoolbox/Medit" target="_blank">Medit vizualisation</a>
   > * <a href="https://wias-berlin.de/software/tetgen/tetview.tutorial.html#Installation" target="_blank">TetView Tutorial</a>
   > * <a href="http://web.mit.edu/tetgen_v1.4.1/tetgen-manual.pdf" target="_blank">TetGen : TetGen, Medit, setup...</a> 

