###### about ######

This repository reports pairs of point clouds (.xyz) and associated envelope meshs (.off). This is used to process voronoi tessellations with **geogram** and the command **./compute_rvd**