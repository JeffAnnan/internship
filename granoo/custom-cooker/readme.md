###### Introduction ######
Welcome to the modified version of the cooker program able to export off and xyz files. 
To export these 2 files you can change the `Type` option of the `_SaveDomain` standard plugin. 

You can take a look at the two last plugins of the `inp/CookingBook-polyhedron.inp` input file.

To run this example, you must : 
 - update your granoo repo with `svn update` at the root of your granoo directory 
 - rebuild your granoo repo with `make && make install` in your build directory

Now, to run the cooker, just type here : 
```
granoo-cooker ./inp/CookingBook-polyhedron.inp
```

If you want to change the polyhedron skin, you can change the input off file in the first plugin of the `inp/CookingBook-polyhedron.inp` *.inp file.

