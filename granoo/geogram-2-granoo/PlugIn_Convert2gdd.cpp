#include "GranOO/libCore/Problem.hpp"
#include "GranOO/libCore/Domain.hpp"
#include "GranOO/libDEM/SupportShapeT.hpp"
#include "GranOO/libUtil/Path.hpp"
#include "PlugIn_Convert2gdd.hpp"


#include <boost/filesystem/fstream.hpp>


#include <fstream>
#include <sstream>


#include <unordered_set>
using namespace std;
#include "boost/filesystem.hpp"   // C++17: #include <filesystem>
namespace fs = boost::filesystem; // C++17: std::filesystem`



REGISTER_GRANOO_PLUGIN(PlugIn_Convert2gdd);


PlugIn_Convert2gdd::PlugIn_Convert2gdd() 
  :Util::PlugInInterface<PlugIn_Convert2gdd>(),
   fileName_("")
{
}

PlugIn_Convert2gdd::~PlugIn_Convert2gdd()
{
}


void
PlugIn_Convert2gdd::ParseXml()
{

  Util::XmlParser& parser = Util::XmlParser::Get();
  parser.ReadAttribute(Attr::GRANOO_OPTIONAL, "FileName", fileName_);

}


void
PlugIn_Convert2gdd::Init() 
{  
}


void
PlugIn_Convert2gdd::Run() 
{
    const size_t lastindex = fileName_.find_last_of(".");
    const std::string rawname = fileName_.substr(0, lastindex);
    int number_of_instanciate_mesh = 0;
    
    //read all files one by oneand fill the std::map
    {
        std::string path = "outputSplitter";
        for (const auto & entry : fs::directory_iterator(path)){
            std::cout << entry << std::endl;
            std::map<int,  Geom::Point> map;
            
                //read vertices
                // open the file and manage potential errors
                boost::filesystem::path file(entry);
                boost::filesystem::ifstream ofs(file);
                AssertMsg(ofs.is_open(), "Can't open file ");
                
                // read the *.off header
                std::string off_type;
                int num_vert, num_face, num_edge;
                ofs >> off_type >> num_vert >> num_face >> num_edge;
                
                // now, read line by line
                for (int i = 0; i < num_vert; ++i)
                {
                // read the coordinates
                int index;
                double x,y,z;
                index=i;
                ofs >> x >> y >> z;
                
                // and fill the map
                map[index] = Geom::Point(x,y,z);
                }
                std::cout << " - find "<< map.size() << " nodes" << std::endl;
          
                
                //read the volume
                int num_vert_face; //correspond to the number of vertices in each face
                unordered_set<int> verticesList; //list of vertices in a volume without duplicates
                std::string line; //line by line of faces
                std::string vertices_of_a_line; //vertices in face line
                //read all faces
                while (getline(ofs, line)) {
                    stringstream check1(line); //convert line to stream
                    ofs >> num_vert_face; //escape the first integer of each line that we don't want
                    //read each vertices in a face line
                    while(getline(check1, vertices_of_a_line, ' ')){
                        // object from the class stringstream
                        stringstream string_to_stream(vertices_of_a_line);
                        // The object has the value 12345 and stream
                        // it to the integer x
                        int stream_to_int = 0;
                        string_to_stream >> stream_to_int;
                        verticesList.insert(stream_to_int);
                    }
                }
                //we got the list of all vertices in the current volume
                //read all these vertices
                unordered_set<int> :: iterator itr;
                std::map<int, Geom::Vector> granoo_points; //map of granoo points
                int point_index = 0;
           
                for (itr = verticesList.begin(); itr != verticesList.end(); itr++){
                    // ok, get the related point instances thanks to the previous map
                    granoo_points[point_index] = map[*itr].ToVector();
                    point_index ++;
                }
              
                //read all granoo point to compute the centroid (geometrical center) of the volume
                Geom::Vector sum; //sum to compute the centroid
                for (int i = 0; i < granoo_points.size(); ++i){
                    sum += granoo_points[i];
                }
                //compoute the centroid
                const Geom::Vector c = (sum)/double(granoo_points.size());
            
            
                // now, creating a stringstream that store the polyhedron in the OFF format
                boost::filesystem::path fileI(entry);
                boost::filesystem::ifstream ofse(fileI);
                std::string lineOutputStream; //line by line of .off file
                std::stringstream off_ss;
                int zone_selector = 0 ; //selector which zone of .off file we want to put in stringstream
                while (getline(ofse, lineOutputStream)) {
                    if (!(zone_selector < 2 || zone_selector > num_vert+1)) {
                        const Geom::Vector& pts = map[zone_selector-2].ToVector();
                        off_ss << pts.x-c.x   << ' ' << pts.y-c.y << ' '  << pts.z-c.z << '\n';
                    }else{
                        off_ss << lineOutputStream << '\n';
                    }
                    zone_selector ++;
                }
                //off_ss << ofse.rdbuf();
                // now create a discrete element of polyhedron type
                DEM::ElementT<Shape::Polyhedron> *body =
                  new DEM::ElementT<Shape::Polyhedron>(c.ToPoint());
                // read the off string stream
                body->GetPolyhedronCore().ReadOFFFile(off_ss);
                // set a mass density to the discrete element arbitraty fixed to 1000 kg/m^3
                body->SetDensity(1000.);
            
            number_of_instanciate_mesh ++;
        }
        std::cout << " - instanciate "<<  number_of_instanciate_mesh << " polyhedron elements" << std::endl;
    }
}



