





#ifndef _PlugIn_Convert2gdd_hpp_
#define _PlugIn_Convert2gdd_hpp_

#include <string>
#include <iostream>
#include <fstream>

#include "GranOO/Common.hpp"
#include "GranOO/libUtil/PlugIn.hpp"


class PlugIn_Convert2gdd: public Util::PlugInInterface<PlugIn_Convert2gdd>
{
  
public: 
  DECLARE_CUSTOM_GRANOO_PLUGIN(Convert2gdd);

  PlugIn_Convert2gdd();
  ~PlugIn_Convert2gdd();

  void ParseXml();  
  void Init();
  void Run();  
  
 public: 
  
  

private: 
  std::string fileName_;
};

#endif
