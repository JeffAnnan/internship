

#include "GranOO/libCore/Problem.hpp"
#include "GranOO/libCore/Domain.hpp"
#include "GranOO/libDEM/SupportShapeT.hpp"
#include "PlugIn_Convert2xyz.hpp"

REGISTER_GRANOO_PLUGIN(PlugIn_Convert2xyz);


PlugIn_Convert2xyz::PlugIn_Convert2xyz() 
  :Util::PlugInInterface<PlugIn_Convert2xyz>(),
   fileName_("")
{
}

PlugIn_Convert2xyz::~PlugIn_Convert2xyz()
{
}


void
PlugIn_Convert2xyz::ParseXml()
{

  Util::XmlParser& parser = Util::XmlParser::Get();
  parser.ReadAttribute(Attr::GRANOO_OPTIONAL, "FileName", fileName_);

}


void
PlugIn_Convert2xyz::Init() 
{  
}


void
PlugIn_Convert2xyz::Run() 
{
  // open file
  std::ofstream file;
  file.open(fileName_.c_str());
  assert(file.is_open());
  file.precision(10);

  // get the global discrete element set
  Core::SetOf<DEM::DiscreteElement >& el_set = Core::SetOf<DEM::DiscreteElement >::Get();

  // parse all the discrete element of the global discrete element set
  for (auto &el : el_set)
    {
      const Geom::Point& p = el->GetCenter();      
      file << p.x << '\t' << p.y << '\t' << p.z <<  "\n";
    }

  file.close();
}



