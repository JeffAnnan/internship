





#ifndef _PlugIn_Convert2xyz_hpp_
#define _PlugIn_Convert2xyz_hpp_

#include <string>
#include <iostream>
#include <fstream>

#include "GranOO/Common.hpp"
#include "GranOO/libUtil/PlugIn.hpp"


class PlugIn_Convert2xyz: public Util::PlugInInterface<PlugIn_Convert2xyz>
{
  
public: 
  DECLARE_CUSTOM_GRANOO_PLUGIN(Convert2xyz);

  PlugIn_Convert2xyz();
  ~PlugIn_Convert2xyz();

  void ParseXml();  
  void Init();
  void Run();  
  
 public: 
  
  

private: 
  std::string fileName_;
};

#endif
