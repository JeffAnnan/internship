###### about ######

This repository reports lgdd files from GranOO. Theses files are splitted meshs obtained by the plugin 'geogram-2-granoo'.
* `benchCubeHole_1000_cells.lgdd` is a cube with two holes and cylinders from side to side : splitted in **1093 voronoi cells**. The simulation on these cells works well -> :white_check_mark:
* `bunny_1000_cells.lgdd` is the **Stanford Bunny** : splitted in **1099 voronoi cells**. The simulation on these cells works well -> :white_check_mark:
* `bunny_20000_cells.lgdd ` is the **Stanford Bunny** : splitted in **20264 voronoi cells**. The simulation on these cells starts but not tried until the end-> :question:
* `cubeCylinder_1000_cells.lgdd` is a cube with cylinders from side to side : splitted in **1083 voronoi cells**. The simulation on these cells works **but** the mesh is oversized (order of magnitude : 200x200 GranOO ground instead of 2x2 for the bunny). So the settings are quite difficult and disproportionate leading to extra-long simulations -> :interrobang:
* `bunny_50000_cells.lgdd ` is the **Stanford Bunny** : splitted in **50730 voronoi cells**. The simulation on these cells doesn't work. **ERROR 'Can't compute position, the mass is null**-> :heavy_multiplication_x:
* `frog_10000_cells.lgdd ` is the a **frog** : splitted in **10069 voronoi cells**. The simulation on these cells doesn't work. **ERROR 'Can't compute position, the mass is null**-> :heavy_multiplication_x:
* `frog_20000_cells.lgdd ` is the a **frog** : splitted in **20237 voronoi cells**. The simulation on these cells doesn't work. **ERROR 'Can't compute position, the mass is null**-> :heavy_multiplication_x: