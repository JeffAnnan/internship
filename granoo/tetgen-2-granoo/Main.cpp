// This file is part of GranOO, a workbench for DEM simulation.
//
// Author(s)    : - Jean-luc CHARLES I2M-DuMAS/ENSAM Talence France
//                  <jean-luc.charles@ensam.eu>
//                - Damien ANDRE     SPCTS/ENS Ceramique industrielle, Limoges France
//                  <damien.andre@unilim.fr>
//                - Jeremie GIRARDOT I2M-DuMAS/ENSAM Talence France
//                  <jeremie.girardot@ensam.eu>
//                - Cedric Hubert    LAMIH/UVHC Valenciennes France
//                  <cedric.hubert@univ-valenciennes.fr>
//                - Ivan IORDANOFF   I2M-DuMAS-MPI/ENSAM Talence France
//                  <ivan.iordanoff@ensam.eu>
//
// Copyright (C) 2008-2016 JL. CHARLES, D. ANDRE, I. IORDANOFF, J. GIRARDOT
//
//
//
//
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


//
// Main for generic GranOO application
//

#include "GranOO/Common.hpp"

int main(int argc, char * argv[])
{
  Core::Problem::Get().Run(argc, argv);
  return 0;
}
