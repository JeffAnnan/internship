

#include "GranOO/libCore/Problem.hpp"
#include "GranOO/libCore/Domain.hpp"
#include "GranOO/libDEM/SupportShapeT.hpp"
#include "GranOO/libUtil/Path.hpp"
#include "PlugIn_Convert2gdd.hpp"


#include <boost/filesystem/fstream.hpp>

REGISTER_GRANOO_PLUGIN(PlugIn_Convert2gdd);


PlugIn_Convert2gdd::PlugIn_Convert2gdd() 
  :Util::PlugInInterface<PlugIn_Convert2gdd>(),
   fileName_("")
{
}

PlugIn_Convert2gdd::~PlugIn_Convert2gdd()
{
}


void
PlugIn_Convert2gdd::ParseXml()
{

  Util::XmlParser& parser = Util::XmlParser::Get();
  parser.ReadAttribute(Attr::GRANOO_OPTIONAL, "FileName", fileName_);

}


void
PlugIn_Convert2gdd::Init() 
{  
}


void
PlugIn_Convert2gdd::Run() 
{  
  const size_t lastindex = fileName_.find_last_of("."); 
  const std::string rawname = fileName_.substr(0, lastindex);
  
  
  // read *.node file and fill the std::map  
  std::map<int,  Geom::Point> map;
  {
    
    // open the file and manage potential errors 
    std::string fileName = rawname + ".node";
    std::cout << "Begin read .node file '"<< fileName << "'" << std::endl;
    AssertMsg(Util::Path::CheckFileExtension(fileName, ".node"),
	      fileName + " has invalid extension for node file");
    
    boost::filesystem::path file(fileName);  
    boost::filesystem::ifstream ofs(file);
    AssertMsg(ofs.is_open(), "Can't open file " + fileName);
    
    // read the *.node header and manage potential errors 
    int el_num, dim, attr_num, bound;
    ofs >> el_num >> dim >> attr_num >> bound;
    AssertMsg(dim == 3, "incorrect dimension, must be 3");
    AssertMsg(attr_num == 0, "attribute number must be zero"); 
    
    // now, read line by line 
    for (int i = 0; i < el_num; ++i)
      {
	// read the coordinates 
	int index, bound;
	double x,y,z;
	ofs >> index >> x >> y >> z >> bound;
	
	// and fill the map 
	map[index] = Geom::Point(x,y,z);
      }
    std::cout << " - find "<< map.size() << " nodes" << std::endl;
  }

  // read *.ele file and fill the std::map  
  {
    // open the file and manage potential errors 
    std::string fileName = rawname + ".ele";
    std::cout << "Begin read .ele file '"<< fileName << "'" << std::endl;
    boost::filesystem::path file(fileName);  
    boost::filesystem::ifstream ofs(file);
    AssertMsg(ofs.is_open(), "Can't open file " + fileName);

    // read the *.ele header 
    int tetra_num, node_num, attr_num;
    ofs >> tetra_num >> node_num >> attr_num;

    // now, read line by line 
    for (int i = 0; i < tetra_num; ++i)
      {
	// read the node index of the related tetrahedron (and manage potential errors)
	int index, tetra1_index, tetra2_index, tetra3_index, tetra4_index;
	ofs >> index >> tetra1_index >> tetra2_index >> tetra3_index >> tetra4_index;
	InternAssert(map.count(tetra1_index) == 1);
	InternAssert(map.count(tetra2_index) == 1);
	InternAssert(map.count(tetra3_index) == 1);
	InternAssert(map.count(tetra4_index) == 1);
	
	// ok, get the related point instances thanks to the previous map
	const Geom::Vector& p1 = map[tetra1_index].ToVector();
	const Geom::Vector& p2 = map[tetra2_index].ToVector();
	const Geom::Vector& p3 = map[tetra3_index].ToVector();
	const Geom::Vector& p4 = map[tetra4_index].ToVector();
	
	// compute the geometrical center of the tetrahedron
	const Geom::Vector c = (p1+p2+p3+p4)/4.;
	
	// now, creating on-the-fly a stringstream that store the tetrahedron in the OFF format
	std::stringstream off_ss;
	off_ss << "OFF\n";
	
	// ok, let's fill the off string stream. The center of the tetrahedron is used for translating
	// the reference because, in GranOO, the center of a polyhedron is always (0,0,0)
	const int num_vertex = 4;
	const int num_face   = 4;
	const int num_edge   = 0;
	off_ss << num_vertex << ' ' << num_face << ' ' << num_edge << '\n';
	off_ss << p1.x-c.x   << ' ' << p1.y-c.y << ' '  << p1.z-c.z << '\n';
	off_ss << p2.x-c.x   << ' ' << p2.y-c.y << ' '  << p2.z-c.z << '\n';
	off_ss << p3.x-c.x   << ' ' << p3.y-c.y << ' '  << p3.z-c.z << '\n';
	off_ss << p4.x-c.x   << ' ' << p4.y-c.y << ' '  << p4.z-c.z << '\n';
	
	off_ss << "3 0 1 2\n";
	off_ss << "3 0 1 3\n";
	off_ss << "3 1 2 3\n";
	off_ss << "3 3 2 0\n";
	
	// now create a discrete element of polyhedron type 
	DEM::ElementT<Shape::Polyhedron> *body =
	  new DEM::ElementT<Shape::Polyhedron>(c.ToPoint());
	
	// read the off string stream
	body->GetPolyhedronCore().ReadOFFFile(off_ss);
	
	// set a mass density to the discrete element arbitraty fixed to 1000 kg/m^3
	body->SetDensity(1000.);
	
      }
    std::cout << " - instanciate "<< tetra_num << " tetra elements" << std::endl;
  }
}




